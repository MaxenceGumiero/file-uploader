import asyncHandler from 'express-async-handler'
import generateJsonWebToken from '../utils/generateJsonWebToken.js'
import User from '../models/userModel.js'

/* This function checks the content of the body, if the entered fields are correct, this returns all user's
parameters (excepts his password for security reasons) as json. If the credentials are incorrect, this trigger
an error */
const signInUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body
  const user = await User.findOne({ email })
  if (user && (await user.matchPassword(password))) {
    res.json({
      _id: user._id,
      email: user.email,
      isAdmin: user.isAdmin,
      token: generateJsonWebToken(user._id)
    })
  } else {
    res.status(401)
    throw new Error(`Your email address or your password is invalid.`)
  }
})

/* This function checks the content of the body, if the entered fields are correct, this creates an user object
with the content of the body, then, it returns all its parameters (excepts its password for security reasons)
as json. If the email address typed by the user has already been taken, this trigger an error. */
const signUpUser = asyncHandler(async (req, res) => {
  const {
    email,
    password
  } = req.body
  const userExists = await User.findOne({ email })
  if (userExists) {
    res.status(400)
    throw new Error(`This email address has already been taken.`)
  }
  const user = await User.create({
    email,
    password
  })
  if (user) {
    res.status(201).json({
      _id: user._id,
      email: user.email,
      isAdmin: user.isAdmin,
      token: generateJsonWebToken(user._id)
    })
  } else {
    res.status(400)
    throw new Error(`It seems that the fields you entered are invalid.`)
  }
})

/* This function checks if there is a current user, if there is, it returns all parameters of the current user (excepts
his password for security reasons) as json. */
const getUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id)
  if (user) {
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    })
  } else {
    res.status(404)
    throw new Error(`It seems that this user does not exists.`)
  }
})

/* This function checks if there is a current user, if there is, if the entered fields are correct, it edits the current
user's profile with the content of the body, then, it returns all parameters of the current user (excepts his password
for security reasons) as json. */
const editUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id)
  if (user) {
    user.name = req.body.name || user.name
    user.email = req.body.email || user.email
    if (req.body.password) {
      user.password = req.body.password
    }
    const updatedUser = await user.save()
    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
      token: generateJsonWebToken(updatedUser._id),
    })
  } else {
    res.status(404)
    throw new Error('It seems that this user does not exists.')
  }
})

/* This function returns all users objects and their properties (except their passwords for security reasons) as
json. */
const getUsers = asyncHandler(async (req, res) => {
  const users = await User.find({}).select('-password')
  res.json(users)
})

/* This function checks if there is any user object id in the parameters of the request, if there is, it returns one
user objects and its properties (except its password for security reasons) as json. */
const getUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id).select('-password')

  if (user) {
    res.json(user)
  } else {
    res.status(404)
    throw new Error('It seems that this user does not exists.')
  }
})

/* This function checks if there is any user object id in the parameters of the request, if there is, and if the entered
fields are correct, it edits the user object with the content of the body, then, it returns all parameters of the
current user (excepts his password for security reasons) as json. */
const editUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id)
  if (user) {
    user.name = req.body.name || user.name
    user.email = req.body.email || user.email
    user.isAdmin = req.body.isAdmin
    const updatedUser = await user.save()
    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
    })
  } else {
    res.status(404)
    throw new Error('It seems that this user does not exists.')
  }
})

/* This function checks if there is any user object id in the parameters of the request, if there is, and if the entered
fields are correct, it removes the user object. */
const removeUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id)
  if (user) {
    await user.remove()
    res.json({ message: 'This user has successfully been removed.' })
  } else {
    res.status(404)
    throw new Error('It seems that this user does not exists.')
  }
})

export {
  signInUser,
  signUpUser,
  getUserProfile,
  editUserProfile,
  getUsers,
  getUser,
  editUser,
  removeUser
}

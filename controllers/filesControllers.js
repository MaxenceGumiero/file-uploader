import asyncHandler from 'express-async-handler'
import File from '../models/fileModel.js'

/* This function returns all files objects and their properties (except their passwords for security reasons) as
json. */
const getFiles = asyncHandler(async (req, res) => {
  const files = await File.find({}).populate('user', '-password')
  res.json(files)
})

/* This function checks if there is any file object id in the parameters of the request, if there is, it returns one
file objects and its properties as json. */
const getFile = asyncHandler(async (req, res) => {
  const file = await File.findById(req.params.id).populate('user', '-password')
  if (file) {
    res.json(file)
  } else {
    res.status(404)
    throw new Error(`It seems that this file does not exists.`)
  }
})

/* This function checks the content of the body, if the entered fields are correct, this creates an file object
with the content of the body, then, it returns all its parameters */
const addFile = asyncHandler(async (req, res) => {
  const {
    name,
    description,
  } = req.body
  const file = await File.create({
    name,
    description,
    path: req.file.path,
    user: req.user._id
  })
  if (file) {
    res.status(201).json({
      _id: file._id,
      name: file.name,
      description: file.description,
      path: file.path,
      user: file.user
    })
  } else {
    res.status(400)
    throw new Error(`It seems that the fields you entered are invalid.`)
  }
})

/* This function checks if there is any file object id in the parameters of the request, if there is, and if the entered
fields are correct, it edits the file object with the content of the body, then, it returns all parameters of the
current file as json. */
const editFile = asyncHandler(async (req, res) => {
  const {
    name,
    description,
  } = req.body
  const {
    path
  } = req.file
  const file = await File.findById(req.params.id)
  if (file) {
    file.name = name
    file.description = description
    file.path = path
    const editedFile = await file.save()
    res.json(editedFile)
  } else {
    res.status(404)
    throw new Error(`It seems that this file does not exists.`)
  }
})

/* This function checks if there is any file object id in the parameters of the request, if there is, and if the entered
fields are correct, it removes the file object. */
const removeFile = asyncHandler(async (req, res) => {
  const file = await File.findById(req.params.id)
  if (file) {
    await file.remove()
    res.json({ message: `This file has successfully been removed.` })
  } else {
    res.status(404)
    throw new Error(`It seems that this file does not exists.`)
  }
})

export {
  getFiles,
  getFile,
  addFile,
  editFile,
  removeFile
}

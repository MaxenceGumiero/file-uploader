import path from 'path'
import express from 'express'
import cors from 'cors'
import dotenv from 'dotenv'
import morgan from 'morgan'
import errorHandler from './middleware/errorMiddleware.js'
import connectToMongoDB from './config/mongooseConfig.js'

/* Calls the application routes. */
import filesRoutes from './routes/filesRoutes.js'
import usersRoutes from './routes/usersRoutes.js'

/* Imports the application main configuration file */
dotenv.config({ path: './config/config.env' })

/* Calls the function that connects this application to my MongoDB Cluster */
connectToMongoDB()

/* Creates a new Express.js server */
const app = express()

/* Checks if this application is is development or production mode, then, uses Morgan library. */
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

app.use(express.json())

/* Application routes base urls */
app.use('/api/files', filesRoutes)
app.use('/api/users', usersRoutes)

/* Calls the errorHandler function to catch all server errors. */
app.use(errorHandler)

/* Allows this application to read and writes static files from a specific folder. */
const __dirname = path.resolve()
app.use('/static/files', express.static(path.join(__dirname, '/files')))

/* Allows frontend applications to use this application. */
app.use(cors())

/* Attributes the 5000 to the server port if it has not been set in the application main configuration file. */
const PORT = process.env.PORT || 5000

/* Launches the server. */
app.listen(PORT, console.log(`Express.js listening from port ${PORT} in ${process.env.NODE_ENV} mode...`))


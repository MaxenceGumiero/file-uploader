import jwt from 'jsonwebtoken'
import asyncHandler from 'express-async-handler'
import User from '../models/userModel.js'

// This function checks if there is any JsonWebToken in the request headers, and checks if it is valid or not.
const authMiddleware = asyncHandler(async (req, res, next) => {
  let jsonWebToken
  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')
  ) {
    try {
      jsonWebToken = req.headers.authorization.split(' ')[1]
      const decoded = jwt.verify(jsonWebToken, process.env.JWT_SECRET)
      req.user = await User.findById(decoded.id).select('-password')
      next()
    } catch (error) {
      console.error(error)
      res.status(401)
      throw new Error('You are not signed in.')
    }
  }
  if (!jsonWebToken) {
    res.status(401)
    throw new Error(`You are not signed in.`)
  }
})

export default authMiddleware

/* This function checks if there is a current user and if he's admin or not. */
const adminMiddleware = (req, res, next) => {
  if (req.user && req.user.isAdmin) {
    next()
  } else {
    res.status(401)
    throw new Error('Not authorized as an admin')
  }
}

export default adminMiddleware

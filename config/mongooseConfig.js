import mongoose from 'mongoose'

// This function connects this application to my MongoDB Cluster.
const connectToMongoDB = async () => {
  try {
    const connect = await mongoose.connect(process.env.MONGO_URI, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
    })
    console.log(`MongoDB listening from host ${connect.connection.host}...`)
  } catch (error) {
    console.error(`Error: ${error.message}`)
    process.exit(1)
  }
}

export default connectToMongoDB

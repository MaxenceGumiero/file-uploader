import mongoose from 'mongoose'

const fileSchema = mongoose.Schema(
  {
    name: {
      type: String,
      maxLength: 100,
      required: true
    },
    description: {
      type: String,
      maxLength: 255,
      required: true
    },
    path: {
      type: String,
      required: true
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    }
  },
  {
    timestamps: true
  }
)

const File = mongoose.model('File', fileSchema)

export default File

import mongoose from 'mongoose'
import bcryptjs from 'bcryptjs'

const userSchema = mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      maxLength: 100,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    isAdmin: {
      type: Boolean,
      required: true,
      default: false
    }
  },
  {
    timestamps: true
  }
)

/* This function check if the entered password does match or not. */
userSchema.methods.matchPassword = async function (enteredPassword) {
  return bcryptjs.compare(enteredPassword, this.password)
}

/* This function check if the entered password has been modified or not, then, it encrypts it. */
userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next()
  }

  const salt = await bcryptjs.genSalt(10)
  this.password = await bcryptjs.hash(this.password, salt)
})

const User = mongoose.model('User', userSchema)

export default User

import express from 'express'
const router = express.Router()
import { signInUser, signUpUser, getUserProfile, editUserProfile, getUsers, getUser, editUser, removeUser } from '../controllers/usersControllers.js'
import adminMiddleware from '../middleware/adminMiddleware.js'
import authMiddleware from '../middleware/authMiddleware.js'

/* These routes are all user model related routes. Some routes required to be signed in or be an admin to be used. */
router
  .route('/sign-in')
  .post(signInUser)
router
  .route('/sign-up')
  .post(signUpUser)
router
  .route('/profile')
  .get(authMiddleware, getUserProfile)
  .put(authMiddleware, editUserProfile)
router
  .route('/')
  .get(authMiddleware, adminMiddleware, getUsers)
router
  .route('/:id')
  .get(authMiddleware, adminMiddleware, getUser)
  .put(authMiddleware, adminMiddleware, editUser)
  .delete(authMiddleware, adminMiddleware, removeUser)


export default router

import path from "path"
import express from 'express'
import mongoose from 'mongoose'
import multer from 'multer'
import { getFiles, getFile, removeFile, addFile, editFile } from '../controllers/filesControllers.js'
import authMiddleware from '../middleware/authMiddleware.js'
const router = express.Router()

/* Put all files in the /static/files folder then renames files by a random MongoDB id. */
const storage = multer.diskStorage({
  destination(req, file, cb) {
    if (req.user) {
      cb(null, `./static/files/`)
    }
  },
  filename(req, file, cb) {
    cb(null, `${new mongoose.Types.ObjectId().toString()}${path.extname(file.originalname)}`)
  }
})

/* This function checks if the file type is good or not. */
const fileTypeFilter = (file, cb) => {
  const filetypes = /flac|iso|jpeg|jpg|mp3|mp4|mkv|pdf|png|rar|wav|wmv|zip/
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase())
  const mimetype = filetypes.test(file.mimetype)

  if (extname && mimetype) {
    return cb(null, true)
  } else {
    return cb(null, false)
  }
}

/* Uses the fileTypeFilter function then saves the uploaded file to the storage. */
const upload = multer({
  storage,
  fileFilter: function (req, file, cb) {
    fileTypeFilter(file, cb)
  },
})


/* These routes are all file model related routes. Some routes required to be signed in or be an admin to be used. */
router
  .route('/')
  .get(getFiles)
  .post(authMiddleware, upload.single('path'), addFile)
router
  .route('/:id')
  .get(authMiddleware, getFile)
  .delete(authMiddleware, removeFile)
  .put(authMiddleware, upload.single('path'), editFile)

export default router

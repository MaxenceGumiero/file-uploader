import jwt from 'jsonwebtoken'

/* This function generates a valid JsonWebToken for 30 days. */
const generateJsonWebToken = id => {
  return jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: '30d',
  })
}

export default generateJsonWebToken
